# Obligatorisk oppgave 1 - C-programmering med prosesser, tr�der og synkronisering

Denne oppgaven best�r av f�lgende laboppgaver fra kompendiet:

* 4.5.b (Lage nye prosesser og enkel synkronisering av disse)
* 5.6.a (Lage nye tr�der og enkel semafor-synkronisering av disse)
* 5.6.b (Flere Producere og Consumere)
* 6.10.a (Dining philosophers)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

* Ola Eksempel
* Kari Mal

## Kvalitetssikring

Vi brukte [ navn p� verkt�y for linting, statisk analyse eller lignende ] for � kvalitetssikre koden. 

[..] foreslo at vi burde ha fikset p� [..]. Etter � ha rettet opp i dette ved � [..] fikk vi ingen feilmeldinger.


[..] kan installeres p� f�lgende m�te / med f�lgende kommandoer: [..]

For � utf�re eksakt samme kvalitetssikring som vi har gjort, gj�r f�lgende / kj�r f�lgende kommandoer: [..]
